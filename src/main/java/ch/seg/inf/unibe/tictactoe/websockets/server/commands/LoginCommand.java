package ch.seg.inf.unibe.tictactoe.websockets.server.commands;

import ch.seg.inf.unibe.tictactoe.websockets.application.Player;
import ch.seg.inf.unibe.tictactoe.websockets.application.TicTacToe;
import ch.seg.inf.unibe.tictactoe.websockets.server.ServerWebsocket;
import ch.seg.inf.unibe.tictactoe.websockets.server.messages.client.InitializeGameMessage;
import ch.seg.inf.unibe.tictactoe.websockets.server.messages.client.SetTitleMessage;
import ch.seg.inf.unibe.tictactoe.websockets.server.messages.client.SuccessfulLoginMessage;
import ch.seg.inf.unibe.tictactoe.websockets.server.messages.server.LoginMessage;

import javax.websocket.Session;

public class LoginCommand implements Command {

    private final Session session;

    private final LoginMessage loginMessage;

    public LoginCommand(Session session, LoginMessage loginMessage) {
        this.session = session;
        this.loginMessage = loginMessage;
    }

    @Override
    public void execute() {
        System.out.println("Execute: " + this);

        Player player = new Player(loginMessage.username());
        ServerWebsocket.addSession(session, player);
        sendMessage(player);
    }

    public void sendMessage(Player player) {

        if (ServerWebsocket.hasGame(loginMessage.gameId())) {
            TicTacToe ticTacToe = ServerWebsocket.getGame(loginMessage.gameId());

            // Login second player:
            try {
                ticTacToe.addPlayer(player);
                sendSuccessfulLoginCommand(player);
            } catch (TicTacToe.TooManyPlayerException e) {
                SetTitleMessage setTitleMessage = new SetTitleMessage("Too many players for game: " + loginMessage.gameId());
                ServerWebsocket.sendMessage(player, setTitleMessage);
                return;
            }

            // Inform all players that the game is getting started ...
            Player[] players = ticTacToe.getPlayer();
            SetTitleMessage setTitleMessage = new SetTitleMessage(players[0].getName() + " Vs. " + players[1].getName());
            ServerWebsocket.sendMessage(ticTacToe.getPlayer(), setTitleMessage);

            // Send the initial game state to all players ...
            InitializeGameMessage initializeGameMessage = new InitializeGameMessage(
                    loginMessage.gameId(),
                    ServerWebsocket.getSession(ticTacToe.currentPlayer()).getId()
            );
            ServerWebsocket.sendMessage(ticTacToe.getPlayer(), initializeGameMessage);
        } else {
            // Login first player:
            TicTacToe ticTacToe = new TicTacToe();
            try {
                ticTacToe.addPlayer(player);
                sendSuccessfulLoginCommand(player);
            } catch (TicTacToe.TooManyPlayerException e) {
                throw new RuntimeException(e); // unexpected here...
            }
            ServerWebsocket.addGame(loginMessage.gameId(), ticTacToe);

            SetTitleMessage setTitleMessage = new SetTitleMessage("Waiting for an opponent. Please be patient.");
            ServerWebsocket.sendMessage(player, setTitleMessage);
        }
    }

    public void sendSuccessfulLoginCommand(Player player) {
        SuccessfulLoginMessage successfulLoginMessage = new SuccessfulLoginMessage(session.getId(), player.getName());
        ServerWebsocket.sendMessage(player, successfulLoginMessage);
    }

    @Override
    public String toString() {
        return "LoginCommand{" +
                "session=" + session.getId() +
                ", loginMessage=" + loginMessage +
                '}';
    }
}
